<?php

namespace Thiagoprz\Safe2Pay\Http;

use \GuzzleHttp\Client as HttpClient;

/**
 * Class Client
 * @package Thiagoprz\Safe2Pay\Http
 */
class Client
{
    /**
     * @var array
     */
    private static $instances = [];

    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * Client constructor.
     */
    public function __construct($payment = false)
    {
        $this->apiKey = config('safe2pay.environment') == 'production' ? config('safe2pay.production.apiKey') : config('safe2pay.staging.apiKey');
        $urlBase = config('safe2pay.environment') == 'production' ? config('safe2pay.production.urlBase') : config('safe2pay.staging.urlBase');
        if ($payment) {
            $urlBase = str_replace('api.safe2pay', 'payment.safe2pay', $urlBase);
        }
        $this->httpClient = new HttpClient([
            'base_uri' => $urlBase,
        ]);
    }

    /**
     * Headers
     *
     * @return array
     */
    private function headers()
    {
        return [
            'X-API-KEY' => $this->apiKey,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
    }

    /**
     * Request GET
     *
     * @param $url
     * @param array $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($url, $params = [])
    {
        $result = $this->httpClient->request('GET', $url, [
            'headers' => $this->headers(),
            'query' => $params,
        ]);
        return json_decode($result->getBody());
    }

    /**
     * Request POST
     *
     * @param string $url
     * @param array $data
     * @return false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post($url, $data = [])
    {
        $result = $this->httpClient->request('POST', $url, [
            'headers' => $this->headers(),
            'body' => json_encode($data),
        ]);
        return json_decode($result->getBody());
    }

    /**
     * Request PUT
     *
     * @param string $url
     * @param array $data
     * @return false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function put($url, $data = [])
    {
        $result = $this->httpClient->request('PUT', $url, [
            'headers' => $this->headers(),
            'body' => json_encode($data),
        ]);
        return json_decode($result->getBody());
    }

    /**
     * Request DELETE
     *
     * @param string $url
     * @return false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete($url)
    {
        $result = $this->httpClient->request('DELETE', $url, [
            'headers' => $this->headers(),
        ]);
        return json_decode($result->getBody());
    }

    /**
     * @param string $apiKey
     * @return Client
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }



    /**
     * Singletons should not be cloneable.
     */
    protected function __clone() { }

    /**
     * Singletons should not be restorable from strings.
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    /**
     * This is the static method that controls the access to the singleton
     * instance. On the first run, it creates a singleton object and places it
     * into the static field. On subsequent runs, it returns the client existing
     * object stored in the static field.
     *
     * This implementation lets you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static function getInstance($payment = false): Client
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static($payment);
        }

        return self::$instances[$cls];
    }

}
