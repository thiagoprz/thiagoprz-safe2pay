<?php


namespace Thiagoprz\Safe2Pay;


use Illuminate\Support\ServiceProvider;

class Safe2PayServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/safe2pay.php' => config_path('safe2pay.php'),
        ]);
    }

}
