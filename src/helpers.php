<?php
if (!function_exists('getClientIP')) {
    /**
     * Retorna o IP
     * @return string
     */
    function getClientIP():string
    {
        $keys=array('HTTP_CLIENT_IP','HTTP_X_FORWARDED_FOR','HTTP_X_FORWARDED','HTTP_FORWARDED_FOR','HTTP_FORWARDED','REMOTE_ADDR');
        foreach($keys as $k)
        {
            if (!empty($_SERVER[$k]) && filter_var($_SERVER[$k], FILTER_VALIDATE_IP))
            {
                return $_SERVER[$k];
            }
        }
        return "UNKNOWN";
    }
}

if (!function_exists('clearIdentifier')) {
    /**
     * @param $identifier
     * @return string|string[]
     */
    function clearIdentifier($identifier)
    {
        return str_replace(['.', '-', '/'], '', $identifier);
    }
}
