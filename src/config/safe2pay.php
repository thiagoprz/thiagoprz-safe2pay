<?php
return [
    'application' => env('SAFE2PAY_APP', 'Laravel'),
    'environment' => env('SAFE2PAY_ENV', 'staging'),
    'production' => [
        'apiKey' => 'SUA CHAVE',
        'urlBase' => 'https://api.safe2pay.com.br',
    ],
    'staging' => [
        'apiKey' => 'SUA CHAVE',
        'urlBase' => 'https://api.safe2pay.com.br',
    ],
    'log' => 'safe2pay.log',
];
