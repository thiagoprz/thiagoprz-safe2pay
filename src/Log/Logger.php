<?php


namespace Thiagoprz\Safe2Pay\Log;


use Monolog\Handler\StreamHandler;
use Monolog\Logger as MonoLogger;

class Logger
{

    const LOG_INFO = 'INFO';
    const LOG_DEBUG = 'DEBUG';
    const LOG_ERROR = 'ERROR';

    public static function logMessage($message, $level = 'INFO')
    {
        $monolog = new MonoLogger('safe2pay');
        $monolog->pushHandler(new StreamHandler(storage_path('logs/' . config('safe2pay.log'))));
        switch($level) {
            case self::LOG_INFO:
                $monolog->info($message);
                break;
            case self::LOG_DEBUG:
                $monolog->debug($message);
                break;
            case self::LOG_ERROR:
                $monolog->error($message);
                break;
        }
    }
}
