<?php

namespace Thiagoprz\Safe2Pay\Services\Marketplace;


/**
 * Class SubaccountCreateResponse
 * @package Thiagoprz\Safe2Pay\Services\Marketplace
 */
final class SubaccountCreateResponse
{
    public $Id;
    public $Name;
    public $Identity;
    public $Token;
}
