<?php


namespace Thiagoprz\Safe2Pay\Services\Marketplace;


class SubaccountUpdateResponse
{
    public $Id;
    public $Name;
    public $Identity;
    public $Token;
}
