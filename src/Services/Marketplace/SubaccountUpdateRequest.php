<?php


namespace Thiagoprz\Safe2Pay\Services\Marketplace;


final class SubaccountUpdateRequest
{
    /**
     * @var string
     */
    public $Name;

    /**
     * @var string
     */
    public $CommercialName;

    /**
     * @var string
     */
    public $Identity;

    /**
     * @var string
     */
    public $Email;

    /**
     * @var string
     */
    public $TechName;

    /**
     * @var string
     */
    public $TechEmail;

    /**
     * @var string
     */
    public $TechIdentity;

    /**
     * @var boolean
     */
    public $IsPanelRestricted;

    /**
     * @var array
     */
    public $BankData;

    /**
     * @var array
     */
    public $Address;

    /**
     * SubaccountUpdateRequest constructor.
     * @param string $Name
     * @param string $CommercialName
     * @param string $Identity
     * @param string $ResponsibleName
     * @param string $ResponsibleIdentity
     * @param string $Email
     * @param string $TechName
     * @param string $TechEmail
     * @param string $TechIdentity
     * @param boolean $IsPanelRestricted
     * @param array $BankData
     * @param array $Address
     */
    public function __construct($Name, $CommercialName, $Identity, $ResponsibleName, $ResponsibleIdentity, $Email,
                                $TechName, $TechEmail, $TechIdentity, $IsPanelRestricted, $BankData, $Address)
    {
        $this->Name = $Name;
        $this->CommercialName = $CommercialName;
        $this->Identity = $Identity;
//        $this->ResponsibleName = $ResponsibleName;
//        $this->ResponsibleIdentity = $ResponsibleIdentity;
        $this->Email = $Email;
        $this->TechName = $TechName;
        $this->TechEmail = $TechEmail;
        $this->TechIdentity = $TechIdentity;
        $this->IsPanelRestricted = $IsPanelRestricted;
        $this->BankData = $BankData;
        $this->Address = $Address;
    }
}
