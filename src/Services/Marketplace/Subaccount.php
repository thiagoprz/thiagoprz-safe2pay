<?php
namespace Thiagoprz\Safe2Pay\Services\Marketplace;

use Thiagoprz\Safe2Pay\Http\Client;
use Thiagoprz\Safe2Pay\Services\Pagination;

/**
 * Class Subaccount
 * @package Thiagoprz\Safe2Pay\Services\Marketplace
 */
class Subaccount
{

    /**
     * Cadastro de subconta
     *
     * @param SubaccountCreateRequest $data
     * @return SubaccountCreateResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function create(SubaccountCreateRequest $data)
    {
        $client = new Client();
        $result = $client->post('/v2/Marketplace/Add', $data);
        if ($result->HasError) {
            throw new \Exception('Erro ao criar a subconta: ' . json_encode($result));
        }
        return $result->ResponseDetail;
    }

    /**
     * Listar subcontas
     *
     * @param Pagination $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function search(Pagination $params)
    {
        $client = new Client();
        $result = $client->get('/v2/Marketplace/List', $params);
        if ($result->HasError) {
            throw new \Exception('Erro ao listar subcontas: ' . json_encode($result));
        }
        return $result->Objects;
    }

    /**
     * Cadastro de subconta
     *
     * @param SubaccountUpdateRequest $data
     * @param int $id
     * @return SubaccountUpdateResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function update(SubaccountUpdateRequest $data, $id)
    {
        $client = new Client();
        $result = $client->put("/v2/Marketplace/Update?id=$id", $data);
        if ($result->HasError) {
            throw new \Exception('Erro ao criar a subconta: ' . json_encode($result));
        }
        return $result->ResponseDetail;
    }

}

