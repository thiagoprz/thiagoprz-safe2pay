<?php


namespace Thiagoprz\Safe2Pay\Services\Token;

/**
 * Class TokenCreateRequest
 * @package Thiagoprz\Safe2Pay\Services\Token
 */
class TokenCreateRequest
{

    /**
     * @var string
     */
    public $Holder;

    /**
     * @var string
     */
    public $CardNumber;

    /**
     * @var string
     */
    public $ExpirationDate;

    /**
     * @var string
     */
    public $SecurityCode;

    public function __construct($Holder, $CardNumber, $ExpirationDate, $SecurityCode)
    {
        $this->Holder = $Holder;
        $this->CardNumber = $CardNumber;
        $this->ExpirationDate = $ExpirationDate;
        $this->SecurityCode = $SecurityCode;
    }
}
