<?php


namespace Thiagoprz\Safe2Pay\Services\Token;


use Thiagoprz\Safe2Pay\Http\Client;

/**
 * Class Token
 * @package Thiagoprz\Safe2Pay\Services\Token
 */
class Token
{

    /**
     * Serviço responsável por permitir o armazenamento seguro de dados sensíveis de cartão de crédito.
     * @param TokenCreateRequest $createRequest
     * @return string Token gerado
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function create(TokenCreateRequest $createRequest)
    {
        $client = new Client(true);
        $result = $client->post('/v2/token', $createRequest);
        if ($result->HasError) {
            throw new \Exception('Erro ao tokenizar cartão: ' . json_encode($result));
        }
        return $result->ResponseDetail->Token;
    }
}
