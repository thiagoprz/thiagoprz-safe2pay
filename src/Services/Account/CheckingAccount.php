<?php

namespace Thiagoprz\Safe2Pay\Services\Account;

use Thiagoprz\Safe2Pay\Http\Client;

class CheckingAccount
{
    /**
     * @var Client
     */
    private $client;

    /**
     * Transaction constructor.
     *
     * @param string $safe2pay_token
     */
    public function __construct($safe2pay_token)
    {
        $this->client = new Client();
        $this->client->setApiKey($safe2pay_token);
    }

    /**
     * Consulta os depósitos da conta
     *
     * @link https://developers.safe2pay.com.br/references/Account/account_deposit_list
     * @param int $month
     * @param int $year
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getListDeposits(int $month, int $year)
    {
        $response = $this->client->get("/v2/CheckingAccount/GetListDeposits", compact('month', 'year'));
        if ($response->HasError) {
            throw new \Exception("Erro ao realizar consulta de depósitos. Erro: $response->Error");
        }
        return $response->ResponseDetail;
    }
}
