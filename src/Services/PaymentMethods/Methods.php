<?php
namespace Thiagoprz\Safe2Pay\Services\PaymentMethods;

use Thiagoprz\Safe2Pay\Http\Client;

/**
 * Controle de métodos de pagamento
 */
class Methods
{


    /**
     * Lista os métodos de pagamento
     */
    public static function list()
    {
        $client = Client::getInstance();
        return $client->get('v2/MerchantPaymentMethod/List');
    }

    /**
     * Atualiza o método de pagamento
     *
     * @param \Thiagoprz\Safe2Pay\Services\PaymentMethods\Methods\CreditCard|\Thiagoprz\Safe2Pay\Services\PaymentMethods\Methods\Pix $paymentMethod
     */
    public static function update($paymentMethod)
    {
        $client = Client::getInstance();
        return $client->put('v2/PaymentMethod/Update', $paymentMethod);
    }
}
