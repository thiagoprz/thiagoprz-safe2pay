<?php

namespace Thiagoprz\Safe2Pay\Services\PaymentMethods\Methods;

/**
 * Configuração do método de pagamento do cartão de crédito
 */
class CreditCard extends Method
{
    /**
     * @var int[]
     */
    public $PaymentMethod = [
        'Code' => 2,
    ];

    /**
     * @var int
     */
    public $InstallmentLimit;

    /**
     * @var int
     */
    public $MinorInstallmentAmount;

    /**
     * @var string
     */
    public $DescriptionInvoice;

    /**
     * @var array
     */
    public $ListInstallments;

    /**
     * @var bool
     */
    public $IsImmediateAnticipation;

    /**
     * @var bool
     */
    public $Merchant;

    /**
     * @param int $InstallmentLimit
     * @param int $MinorInstallmentAmount
     * @param string $DescriptionInvoice
     * @param array $ListInstallments
     * @param bool $IsImmediateAnticipation
     * @param bool $Merchant
     */
    public function __construct($InstallmentLimit = 12, $MinorInstallmentAmount = 5, $DescriptionInvoice = '', $ListInstallments = [], $IsImmediateAnticipation = false, $Merchant = ['IsPassOnRateDifference' => false])
    {
        $this->InstallmentLimit = $InstallmentLimit;
        $this->MinorInstallmentAmount = $MinorInstallmentAmount;
        $this->DescriptionInvoice = $DescriptionInvoice;
        $this->ListInstallments = $ListInstallments;
        $this->IsImmediateAnticipation = $IsImmediateAnticipation;
        $this->Merchant = $Merchant;
    }


}
