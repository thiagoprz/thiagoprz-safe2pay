<?php

namespace Thiagoprz\Safe2Pay\Services\PaymentMethods\Methods;

/**
 *
 */
class Method implements \JsonSerializable
{

    /**
     * @var array
     */
    public $PaymentMethod = [
        'Code' => 0,
    ];

    /**
     * @var bool
     */
    public $IsEnabled = true;

    /**
     * @return false|mixed|string
     */
    public function jsonSerialize()
    {
        return array_filter(get_object_vars($this));
    }
}
