<?php

namespace Thiagoprz\Safe2Pay\Services\PaymentMethods\Methods;

/**
 * Configuração do método de pagamento do PIX
 */
class Pix extends Method
{
    /**
     * @var int[]
     */
    public $PaymentMethod = [
        'Code' => 6,
    ];
}
