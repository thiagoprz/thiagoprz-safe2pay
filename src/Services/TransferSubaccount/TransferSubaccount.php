<?php
namespace Thiagoprz\Safe2Pay\Services\TransferSubaccount;

use Thiagoprz\Safe2Pay\Http\Client;

/**
 * Class TransferSubaccount
 * @package Thiagoprz\Safe2Pay\Services\TransferSubaccount
 */
class TransferSubaccount
{
    /**
     * @param TransferSubaccountRequest $transferSubaccountRequest
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function create(TransferSubaccountRequest $transferSubaccountRequest)
    {
        $client = new Client();
        $result = $client->post('https://payment.safe2pay.com.br/v2/TransferSubaccount', $transferSubaccountRequest);
        if ($result->HasError) {
            throw new \Exception('Erro ao criar a transferência: ' . json_encode($result));
        }
        return $result->ResponseDetail;
    }
}
