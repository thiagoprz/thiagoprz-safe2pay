<?php


namespace Thiagoprz\Safe2Pay\Services\TransferSubaccount;


/**
 * Class TransferSubaccountRequest
 * @package Thiagoprz\Safe2Pay\Services\TransferSubaccount
 */
class TransferSubaccountRequest
{

    /**
     * @var string
     */
    public $Identity;

    /**
     * @var int
     */
    public $IdentificationDebit;

    /**
     * @var int
     */
    public $IdentificationCredit;

    /**
     * @var float
     */
    public $Amount;

    /**
     * TransferSubaccount constructor.
     * @param $Identity
     * @param $IdentificationDebit
     * @param $IdentificationCredit
     * @param $Amount
     */
    public function __construct($Identity, $IdentificationDebit, $IdentificationCredit, $Amount)
    {
        $this->Identity = $Identity;
        $this->IdentificationDebit = $IdentificationDebit;
        $this->IdentificationCredit = $IdentificationCredit;
        $this->Amount = $Amount;
        $this->Identity = $Identity;
    }
}
