<?php


namespace Thiagoprz\Safe2Pay\Services\Payment\PaymentMethods;


use Thiagoprz\Safe2Pay\Services\Payment\Attributes\PaymentObject;

final class CreditCard extends PaymentObject
{
    /**
     * @var string
     */
    public $IsPreAuthorization = false;

    /**
     * @var string Nome da(o) proprietária(o) do cartão, deve ser preenchido utilizando o método setHolder()
     * @see setHolder()
     */
    public $Holder;

    /**
     * @var string Número do cartão, deve ser preenchido utilizando o método setCardNumber()
     * @see setCardNumber()
     */
    public $CardNumber;

    /**
     * @var string Data de expiração do cartão, deve ser preenchido utilizando o método setExpirationDate()
     * @see setExpirationDate()
     */
    public $ExpirationDate;

    /**
     * @var string Código de segurança do cartão, deve ser preenchido utilizando o método setSecurityCode()
     * @see setSecurityCode()
     */
    public $SecurityCode;

    /**
     * @var string Se o cartão foi tokenizado deve ser preenchido utilizando o método setToken()
     * @see setToken()
     */
    public $Token;

    /**
     * PaymentObject constructor.
     * @param $InstallmentQuantity
     * @param $IsApplyInterest
     * @param $InterestRate
     * @param $SoftDescriptor
     */
    public function __construct($InstallmentQuantity, $IsApplyInterest, $InterestRate, $SoftDescriptor)
    {
        $this->InstallmentQuantity = $InstallmentQuantity;
        $this->IsApplyInterest = $IsApplyInterest;
        $this->InterestRate = $InterestRate;
        $this->SoftDescriptor = $SoftDescriptor;
    }

    /**
     * @return string
     */
    public function getHolder(): string
    {
        return $this->Holder;
    }

    /**
     * @param string $Holder
     */
    public function setHolder(string $Holder): void
    {
        $this->Holder = $Holder;
    }

    /**
     * @return string
     */
    public function getExpirationDate(): string
    {
        return $this->ExpirationDate;
    }

    /**
     * @param string $ExpirationDate
     */
    public function setExpirationDate(string $ExpirationDate): void
    {
        $this->ExpirationDate = $ExpirationDate;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->Token;
    }

    /**
     * @param string $Token
     */
    public function setToken(string $Token): void
    {
        $this->Token = $Token;
        unset($this->CardNumber);
        unset($this->Holder);
        unset($this->ExpirationDate);
        unset($this->SecurityCode);
    }

    /**
     * @return string
     */
    public function getSecurityCode(): string
    {
        return $this->SecurityCode;
    }

    /**
     * @param string $SecurityCode
     */
    public function setSecurityCode(string $SecurityCode): void
    {
        $this->SecurityCode = $SecurityCode;
    }

    /**
     * @return string
     */
    public function getCardNumber(): string
    {
        return $this->CardNumber;
    }

    /**
     * @param string $CardNumber
     */
    public function setCardNumber(string $CardNumber): void
    {
        $this->CardNumber = str_replace([' ', '-', '.'], '', $CardNumber);
        unset($this->Token);
    }
}
