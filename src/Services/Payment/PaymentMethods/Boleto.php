<?php


namespace Thiagoprz\Safe2Pay\Services\Payment\PaymentMethods;


use Thiagoprz\Safe2Pay\Services\Payment\Attributes\PaymentObject;

/**
 * Class Boleto
 * @package Thiagoprz\Safe2Pay\Services\Payment\PaymentMethods
 */
final class Boleto extends PaymentObject
{

    /**
     * @var bool
     */
    public $IsEnabledPartialPayment = false;

    /**
     * @var float
     */
    public $DiscountAmount;

    /**
     * @var string
     */
    public $DiscountType;

    /**
     * @var string
     */
    public $DiscountDue;
    /**
     * @var string
     */
    public $DueDate;
    /**
     * @var string
     */
    public $Instruction;
    /**
     * @var array
     */
    public $Message;
    /**
     * @var float
     */
    public $PenaltyRate;
    /**
     * @var bool
     */
    public $CancelAfterDue;
    /**
     * @var int
     */
    public $DaysBeforeCancel;
    /**
     * @var string
     */
    public $SoftDecriptor;


    /**
     * Boleto constructor.
     * @param string $DueDate
     * @param string $Instruction
     * @param array $Message
     * @param float $PenaltyRate
     * @param float $InterestRate
     * @param bool $CancelAfterDue
     * @param int $DaysBeforeCancel
     */
    public function __construct(string $DueDate, string $Instruction, array $Message, $PenaltyRate, $InterestRate, bool $CancelAfterDue, int $DaysBeforeCancel, string $SoftDecriptor)
    {
        $this->DueDate = $DueDate;
        $this->Instruction = $Instruction;
        $this->Message = $Message;
        $this->PenaltyRate = $PenaltyRate;
        $this->InterestRate = $InterestRate;
        $this->CancelAfterDue = $CancelAfterDue;
        $this->DaysBeforeCancel = $DaysBeforeCancel;
        $this->SoftDecriptor = $SoftDecriptor;
        unset($this->InstallmentQuantity);
    }

    /**
     * @return bool
     */
    public function isEnabledPartialPayment(): bool
    {
        return $this->IsEnabledPartialPayment;
    }

    /**
     * @param bool $IsEnabledPartialPayment
     */
    public function setIsEnabledPartialPayment(bool $IsEnabledPartialPayment): void
    {
        $this->IsEnabledPartialPayment = $IsEnabledPartialPayment;
    }

    /**
     * @return mixed
     */
    public function getDiscountAmount()
    {
        return $this->DiscountAmount;
    }

    /**
     * @param mixed $DiscountAmount
     */
    public function setDiscountAmount($DiscountAmount): void
    {
        $this->DiscountAmount = $DiscountAmount;
    }

    /**
     * @return mixed
     */
    public function getDiscountType()
    {
        return $this->DiscountType;
    }

    /**
     * @param mixed $DiscountType
     */
    public function setDiscountType($DiscountType): void
    {
        $this->DiscountType = $DiscountType;
        if (!$DiscountType) {
            unset($this->DiscountType);
            unset($this->DiscountDue);
            unset($this->DiscountAmount);
        }
    }

    /**
     * @return mixed
     */
    public function getDiscountDue()
    {
        return $this->DiscountDue;
    }

    /**
     * @param mixed $DiscountDue
     */
    public function setDiscountDue($DiscountDue): void
    {
        $this->DiscountDue = $DiscountDue;
    }



}
