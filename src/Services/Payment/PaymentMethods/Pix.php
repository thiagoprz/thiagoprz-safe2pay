<?php


namespace Thiagoprz\Safe2Pay\Services\Payment\PaymentMethods;

use Thiagoprz\Safe2Pay\Services\Payment\Attributes\PaymentObject;

/**
 * Class Pix
 * @package Thiagoprz\Safe2Pay\Services\Payment
 */
final class Pix extends PaymentObject
{
    /**
     * @var int
     */
    public $Expiration;

    /**
     * Pix constructor.
     */
    public function __construct($Expiration = 86400)
    {
        $this->Expiration = $Expiration;
        unset($this->InstallmentQuantity);
        unset($this->IsApplyInterest);
        unset($this->InterestRate);
        unset($this->SoftDescriptor);
    }
}
