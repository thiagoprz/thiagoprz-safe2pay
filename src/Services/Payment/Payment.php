<?php
namespace Thiagoprz\Safe2Pay\Services\Payment;

use Thiagoprz\Safe2Pay\Http\Client;
use Thiagoprz\Safe2Pay\Log\Logger;
use Thiagoprz\Safe2Pay\Services\Payment\Attributes\Customer;
use Thiagoprz\Safe2Pay\Services\Payment\Attributes\Product;
use Thiagoprz\Safe2Pay\Services\Payment\PaymentMethods\Boleto;
use Thiagoprz\Safe2Pay\Services\Payment\PaymentMethods\CreditCard;
use Thiagoprz\Safe2Pay\Services\Payment\PaymentMethods\Pix;

class Payment
{
    /**
     * @var bool
     */
    public $IsSandbox;

    /**
     * @var string
     */
    public $IpAddress;

    /**
     * @var string
     */
    public $Application;

    /**
     * @var string
     */
    public $Vendor;

    /**
     * @var string
     */
    public $CallbackUrl;

    /**
     * @var string
     */
    public $PaymentMethod;

    /**
     * @var string
     */
    public $Reference;

    /**
     * @var mixed
     */
    public $Meta;

    /**
     * @var bool
     */
    public $ShouldUseAntiFraud = false;

    /**
     * @var string
     */
    public $VisitorID = null;

    /**
     * @var Customer
     */
    public $Customer;

    /**
     * @var Product[]
     */
    public $Products;

    /**
     * @var CreditCard|Pix|Boleto
     */
    public $PaymentObject;

    /**
     * CreditCard constructor.
     * @param string $Vendor
     * @param string $CallbackUrl
     * @param string $Reference
     * @param mixed $Meta
     * @param Customer $Customer
     * @param Product[] $Products
     * @param CreditCard|Pix|Boleto $PaymentObject
     */
    public function __construct(string $Vendor, string $CallbackUrl, string $Reference, $Meta, Customer $Customer, array $Products, $PaymentObject)
    {
        $this->IsSandbox = config('safe2pay.environment') == 'staging';
        $this->IpAddress = getClientIP();
        $this->Application = config('safe2pay.application');
        $this->Vendor = $Vendor;
        $this->CallbackUrl = $CallbackUrl;
        $this->Reference = $Reference;
        $this->Meta = $Meta;
        $this->Customer = $Customer;
        $this->Products = $Products;
        $this->PaymentObject = $PaymentObject;
        if ($PaymentObject instanceof Boleto) {
            $this->PaymentMethod = '1';
        } elseif ($PaymentObject instanceof CreditCard) {
            $this->PaymentMethod = '2';
        } elseif ($PaymentObject instanceof Pix) {
            $this->PaymentMethod = '6';
        }
        Logger::logMessage('===================== Criando um novo pagamento =====================');
        Logger::logMessage(json_encode($this));
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function process()
    {
        $client = new Client(true);
        Logger::logMessage('===================== Realizando pagamento =====================');
        $result = $client->post('v2/Payment', $this);
        Logger::logMessage(json_encode($result));
        if ($result->HasError) {
            throw new \Exception('Erro ao realizar o pagamento: ' . json_encode($result));
        }
        return $result->ResponseDetail;
    }

    /**
     * @return bool
     */
    public function isShouldUseAntiFraud(): bool
    {
        return $this->ShouldUseAntiFraud;
    }

    /**
     * @param bool $ShouldUseAntiFraud
     */
    public function setShouldUseAntiFraud(bool $ShouldUseAntiFraud): void
    {
        $this->ShouldUseAntiFraud = $ShouldUseAntiFraud;
    }

    /**
     * @return string
     */
    public function getVisitorID(): ?string
    {
        return $this->VisitorID;
    }

    /**
     * @param string $VisitorID
     */
    public function setVisitorID(?string $VisitorID): void
    {
        $this->VisitorID = $VisitorID;
    }
}
