<?php


namespace Thiagoprz\Safe2Pay\Services\Payment\Attributes;


final class Product
{
    /**
     * @var string
     */
    public $Code;

    /**
     * @var string
     */
    public $Description;

    /**
     * @var string
     */
    public $UnitPrice;

    /**
     * @var string
     */
    public $Quantity;

    /**
     * Product constructor.
     * @param $Code
     * @param $Description
     * @param $UnitPrice
     * @param $Quantity
     */
    public function __construct($Code, $Description, $UnitPrice, $Quantity)
    {
        $this->Code = $Code;
        $this->Description = $Description;
        $this->UnitPrice = $UnitPrice;
        $this->Quantity = $Quantity;
    }
}
