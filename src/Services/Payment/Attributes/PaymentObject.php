<?php


namespace Thiagoprz\Safe2Pay\Services\Payment\Attributes;


class PaymentObject
{
    /**
     * @var string
     */
    public $InstallmentQuantity;

    /**
     * @var string
     */
    public $IsApplyInterest;

    /**
     * @var string
     */
    public $InterestRate;

    /**
     * @var string
     */
    public $SoftDescriptor;
}
