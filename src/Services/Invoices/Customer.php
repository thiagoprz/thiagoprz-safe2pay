<?php


namespace Thiagoprz\Safe2Pay\Services\Invoices;

/**
 * Class Customer
 * @package Thiagoprz\Safe2Pay\Services\Invoices
 */
final class Customer
{
    /**
     * @var string
     */
    public $Name;

    /**
     * @var string
     */
    public $Email;

    /**
     * @var string
     */
    public $Phone;

    /**
     * @var string
     */
    public $Identity;

    /**
     * @var $Address
     */
    public $Address;

    /**
     * Customer constructor.
     * @param $Name
     * @param $Email
     * @param $Phone
     * @param $Identity
     * @param Address $Address
     */
    public function __construct($Name, $Email, $Phone, $Identity, Address $Address)
    {
        $this->Name = $Name;
        $this->Email = $Email;
        $this->Phone = $Phone;
        $this->Identity = $Identity;
        $this->Address = $Address;
    }

}
