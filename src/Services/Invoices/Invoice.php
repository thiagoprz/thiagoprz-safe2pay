<?php


namespace Thiagoprz\Safe2Pay\Services\Invoices;


use Thiagoprz\Safe2Pay\Http\Client;

class Invoice
{

    /**
     * @param InvoiceBoleto|InvoiceCartaoCredito|InvoiceCartaoDebito|InvoicePix $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function create($data)
    {
        $client = new Client();
        $result = $client->post('/v2/SingleSale/Add', $data);
        if ($result->HasError) {
            throw new \Exception('Erro ao criar cobrança: ' . json_encode($result));
        }
        return $result->ResponseDetail;
    }


}
