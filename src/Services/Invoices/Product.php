<?php


namespace Thiagoprz\Safe2Pay\Services\Invoices;


final class Product
{
    /**
     * @var string
     */
    public $Description;

    /**
     * @var int
     */
    public $Quantity;

    /**
     * @var float
     */
    public $UnitPrice;

    /**
     * Product constructor.
     * @param string $Description
     * @param int $Quantity
     * @param float $UnitPrice
     */
    public function __construct($Description, $Quantity, $UnitPrice)
    {
        $this->Description = $Description;
        $this->Quantity = $Quantity;
        $this->UnitPrice = $UnitPrice;
    }
}
