<?php


namespace Thiagoprz\Safe2Pay\Services\Invoices;

/**
 * Class InvoicePix
 * @package Thiagoprz\Safe2Pay\Services\Invoices
 */
final class InvoicePix
{
    /**
     * @var array
     */
    public $PaymentMethods = [
        ['CodePaymentMethod' => 6],
    ];

    /**
     * @var Customer
     */
    public $Customer;

    /**
     * @var Product[]
     */
    public $Products;

    /**
     * @var string
     */
    public $Reference;

    /**
     * @var string
     */
    public $CallbackUrl;

    /**
     * @var string
     */
    public $Emails;

    /**
     * @var string
     */
    public $ExpirationDate;

    /**
     * InvoiceCreateRequest constructor.
     * @param Customer $Customer
     * @param Product[] $Products
     * @param string $Reference
     * @param string $CallbackUrl
     * @param string $Emails
     */
    public function __construct($Customer, $Products, $Reference,
                                $CallbackUrl, $Emails, $ExpirationDate)
    {
        $this->Customer = $Customer;
        $this->Products = $Products;
        $this->Reference = (string)$Reference;
        $this->CallbackUrl = $CallbackUrl;
        $this->Emails = $Emails;
        $this->ExpirationDate = $ExpirationDate;
    }

}
