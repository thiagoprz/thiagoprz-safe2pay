<?php


namespace Thiagoprz\Safe2Pay\Services\Invoices;

/**
 * Class InvoiceCartaoCredito
 * @package Thiagoprz\Safe2Pay\Services\Invoices
 */
final class InvoiceCartaoCredito
{
    /**
     * @var array
     */
    public $PaymentMethods = [
        ['CodePaymentMethod' => 2],
    ];

    /**
     * @var Customer
     */
    public $Customer;

    /**
     * @var Product[]
     */
    public $Products;

    /**
     * @var string
     */
    public $Reference;

    /**
     * @var string
     */
    public $ExpirationDate;

    /**
     * @var string
     */
    public $CallbackUrl;

    /**
     * @var string
     */
    public $Emails;

    /**
     * @var string
     */
    public $DueDate;

    /**
     * @var float
     */
    public $PenaltyAmount;

    /**
     * @var float
     */
    public $InterestAmount;

    /**
     * @var int
     */
    public $InstallmentQuantity;

    /**
     * InvoiceCreateRequest constructor.
     * @param Customer $Customer
     * @param Product[] $Products
     * @param string $Reference
     * @param string $ExpirationDate
     * @param string $CallbackUrl
     * @param string $Emails
     * @param string $DueDate
     * @param float $PenaltyAmount
     * @param float $InterestAmount
     * @param int $InstallmentQuantity
     */
    public function __construct($Customer, $Products, $Reference, $ExpirationDate,
                                $CallbackUrl, $Emails, $DueDate,
                                $PenaltyAmount, $InterestAmount, $InstallmentQuantity)
    {
        $this->Customer = $Customer;
        $this->Products = $Products;
        $this->Reference = (string)$Reference;
        $this->ExpirationDate = $ExpirationDate;
        $this->CallbackUrl = $CallbackUrl;
        $this->Emails = $Emails;
        $this->DueDate = $DueDate;
        $this->PenaltyAmount = $PenaltyAmount;
        $this->InterestAmount = $InterestAmount;
        $this->InstallmentQuantity = $InstallmentQuantity;
    }

}
