<?php


namespace Thiagoprz\Safe2Pay\Services\Invoices;

/**
 * Class Address
 * @package Thiagoprz\Safe2Pay\Services\Invoices
 */
final class Address
{
    /**
     * @var string
     */
    public $Street;

    /**
     * @var int
     */
    public $Number;

    /**
     * @var string
     */
    public $District;

    /**
     * @var string
     */
    public $ZipCode;

    /**
     * @var string
     */
    public $Complement;

    /**
     * @var string
     */
    public $CityName;

    /**
     * @var string
     */
    public $StateInitials;

    /**
     * @var string
     */
    public $CountryName;


    /**
     * Address constructor.
     * @param $Street
     * @param $Number
     * @param $District
     * @param $ZipCode
     * @param $Complement
     * @param $CityName
     * @param $StateInitials
     * @param $CountryName
     */
    public function __construct($Street, $Number, $District, $ZipCode, $Complement, $CityName, $StateInitials, $CountryName)
    {
        $this->Street = $Street;
        $this->Number = $Number;
        $this->District = $District;
        $this->ZipCode =    (string)preg_replace( '/[^0-9]/', '', $ZipCode);
        $this->Complement = $Complement;
        $this->CityName = $CityName;
        $this->StateInitials = $StateInitials;
        $this->CountryName = $CountryName;
    }
}
