<?php


namespace Thiagoprz\Safe2Pay\Services\Invoices;

/**
 * Class InvoiceCartaoDebito
 * @package Thiagoprz\Safe2Pay\Services\Invoices
 */
final class InvoiceCartaoDebito
{
    /**
     * @var array
     */
    public $PaymentMethods = [
        ['CodePaymentMethod' => 4],
    ];

    /**
     * @var Customer
     */
    public $Customer;

    /**
     * @var Product[]
     */
    public $Products;

    /**
     * @var string
     */
    public $Reference;

    /**
     * @var string
     */
    public $ExpirationDate;

    /**
     * @var string
     */
    public $CallbackUrl;

    /**
     * @var string
     */
    public $Emails;

    /**
     * @var string
     */
    public $DueDate;

    /**
     * @var float
     */
    public $PenaltyAmount;

    /**
     * @var float
     */
    public $InterestAmount;

    /**
     * InvoiceCreateRequest constructor.
     * @param Customer $Customer
     * @param Product[] $Products
     * @param string $Reference
     * @param string $ExpirationDate
     * @param string $CallbackUrl
     * @param string $Emails
     * @param string $DueDate
     * @param float $PenaltyAmount
     * @param float $InterestAmount
     */
    public function __construct($Customer, $Products, $Reference, $ExpirationDate,
                                $CallbackUrl, $Emails, $DueDate,
                                $PenaltyAmount, $InterestAmount)
    {
        $this->Customer = $Customer;
        $this->Products = $Products;
        $this->Reference = (string)$Reference;
        $this->ExpirationDate = $ExpirationDate;
        $this->CallbackUrl = $CallbackUrl;
        $this->Emails = $Emails;
        $this->DueDate = $DueDate;
        $this->PenaltyAmount = $PenaltyAmount;
        $this->InterestAmount = $InterestAmount;
    }

}
