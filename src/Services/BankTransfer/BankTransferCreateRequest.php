<?php


namespace Thiagoprz\Safe2Pay\Services\BankTransfer;


class BankTransferCreateRequest
{
    /**
     * @var bool
     */
    public $IsUseCheckingAccount = false;

    /**
     * @var TransferRegister[]
     */
    public $TransferRegisters = [];

    /**
     * BankTransferCreateRequest constructor.
     * @param false $IsUseCheckingAccount
     * @param array $TransferRegisters
     */
    public function __construct($IsUseCheckingAccount = false, $TransferRegisters = [])
    {
        $this->IsUseCheckingAccount = $IsUseCheckingAccount;
        $this->TransferRegisters = $TransferRegisters;
    }
}
