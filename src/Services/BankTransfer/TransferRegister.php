<?php


namespace Thiagoprz\Safe2Pay\Services\BankTransfer;


class TransferRegister
{

    /**
     * @var array
     */
    public $BankData;

    /**
     * @var string
     */
    public $ReceiverName;

    /**
     * @var float
     */
    public $Amount;

    /**
     * @var string
     */
    public $Identity;

    /**
     * @var string
     */
    public $Identification;

    /**
     * @var string
     */
    public $CompensationDate;

    /**
     * @var string
     */
    public $CallbackUrl;

    /**
     * TransferRegister constructor.
     * @param array $BankData
     * @param string $ReceiverName
     * @param float $Amount
     * @param string $Identity
     * @param string $Identification
     * @param string $CompensationDate
     * @param string $CallbackUrl
     */
    public function __construct($BankData, $ReceiverName, $Amount, $Identity, $Identification, $CompensationDate, $CallbackUrl)
    {
        $this->BankData = $BankData;
        $this->ReceiverName = $ReceiverName;
        $this->Amount = $Amount;
        $this->Identity = $Identity;
        $this->Identification = $Identification;
        $this->CompensationDate = $CompensationDate;
        $this->CallbackUrl = $CallbackUrl;
    }
}
