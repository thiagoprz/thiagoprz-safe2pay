<?php


namespace Thiagoprz\Safe2Pay\Services\BankTransfer;


use Thiagoprz\Safe2Pay\Http\Client;
use Thiagoprz\Safe2Pay\Services\Invoices\InvoiceBoleto;
use Thiagoprz\Safe2Pay\Services\Invoices\InvoiceCartaoCredito;

class BankTransfer
{


    /**
     * @param BankTransferCreateRequest $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function create(BankTransferCreateRequest $data)
    {
        $client = new Client();
        $result = $client->post('/v2/transfer', $data);
        if ($result->HasError) {
            throw new \Exception('Erro ao realizar transferência: ' . json_encode($result));
        }
        return $result->ResponseDetail;
    }
}
