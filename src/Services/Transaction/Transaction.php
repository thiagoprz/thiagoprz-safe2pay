<?php


namespace Thiagoprz\Safe2Pay\Services\Transaction;


use Thiagoprz\Safe2Pay\Http\Client;

class Transaction
{
    /**
     * @var Client
     */
    private $client;

    /**
     * Transaction constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param int $IdTransaction
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTransaction(int $IdTransaction)
    {
        $response = $this->client->get("/v2/transaction/Get", ['Id' => $IdTransaction]);
        if ($response->HasError) {
            throw new \Exception('Erro ao realizar consulta da transação. Erro: ' . json_encode($response));
        }
        return $response->ResponseDetail;
    }
}
