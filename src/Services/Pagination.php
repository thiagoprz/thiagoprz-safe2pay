<?php
namespace Thiagoprz\Safe2Pay\Services;

/**
 * Class Pagination
 * @package Thiagoprz\Safe2Pay\Services
 */
class Pagination
{
    /**
     * @var int
     */
    public $PageNumber;

    /**
     * @var int
     */
    public $RowsPerPage;

    /**
     * Pagination constructor.
     * @param $PageNumber
     * @param $RowsPerPage
     */
    public function __construct($PageNumber, $RowsPerPage)
    {
        $this->PageNumber = $PageNumber;
        $this->RowsPerPage = $RowsPerPage;
    }
}
