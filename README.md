PHP Safe2Pay Support Library (Deprecated) 
=

> **Status: Deprecated (Projeto Abandonado)**

Biblioteca criada para dar suporte através de classes PHP à comunicação com a API de serviços da Safe2Pay. 

Instalação
--
Instalação através do composer:

`composer require thiagoprz/safe2pay`

Publicar a configuração da API:

`php artisan vendor:publish --provider="Thiagoprz\Safe2PayServiceProvider" --tag="config"`

Projeto
--
Open source apenas para facilitar a comunicação com a API da Safe2Pay. 



